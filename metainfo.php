<?php

$DealerName = "Gainesville Nissan";
$DealerLocation = "Gainesville, FL";

// Page Specific Meta Descriptions

switch ($post->post_name) {
    case 'home':
        echo "<meta name='Title' content='${DealerName} Service Center'/><meta name='description' content='Get car repairs & maintenance at our service center. We use only original manufacturer parts & certified service technicians so your repairs are done right.'/>";
        break;
    case 'schedule-service':
        echo "<meta name='Title' content='${DealerName} Service Center'/><meta name='description' content='Get car repairs & maintenance at our service center. We use only original manufacturer parts & certified service technicians so your repairs are done right.'/>";
        break;
    case 'services':
        echo "<meta name='Title' content='$DealerName Service Center'/>
    	<meta name='description' content='Get car repairs & maintenance at our service center. We use only original manufacturer parts & certified service technicians so your repairs are done right.'/>";
        break;
    case 'oil-fluid-changes':
        echo "<meta name='Title' content='Schedule Repairs & Maintenance | ${$DealerLocation} Car Repair'/>
        <meta name='description' content='Make an appointment for car repairs, maintenance, or body work online instantly at $DealerName&apos;s service center.'/>";
        break;

    case 'batteries':
        echo "<meta name='Title' content='Our Services | ${$DealerLocation} Car Repair'/>
        <meta name='description' content='Learn more about $DealerName&apos;s service center, including oil changes, alignments, brake repair, and tires.'/>";
        break;
    case 'tires':
        echo "<meta name='Title' content='Get an Oil Change | ${$DealerLocation} Car Repair'/>
        <meta name='description' content='Learn more about oil changes at $DealerName&apos;s service center, then schedule your oil change appointment.'/>";
        break;
    case 'brakes-pads':
        echo "<meta name='Title' content='Get Car Batteries Replaced | ${$DealerLocation} Car Repair'/>
        <meta name='description' content='Learn more about car batteries repair and replacement at $DealerName&apos;s service center, then schedule your appointment.'/>";
        break;
    case 'parts':
        echo "<meta name='Title' content='Get Tire Rotation, Repair, or Replacement | ${$DealerLocation} Car Repair'/>
        <meta name='description' content='Learn more about tire repairs, replacement, alignment and rotation at $DealerName&apos;s service center, then schedule your appointment.'/>";
        break;
    case 'coupons':
        echo "<meta name='Title' content='Get Brake Repairs | ${$DealerLocation} Car Repair'/>
        <meta name='description' content='Learn more about brake repair and pad replacement at $DealerName&apos;s service center, then schedule your oil change appointment.'/>";
        break;
    case 'Learning Center':
        echo "<meta name='Title' content='Coupons for OEM Parts & Car Repair | ${$DealerLocation} Car Repair'/>
        <meta name='description' content='Get coupons and discounts for OEM parts, car repair, and car maintenance at $DealerName&apos;s service center.'/>";
        break;
    case 'faqs':
        echo "<meta name='Title' content='FAQ | ${$DealerLocation} Car Repair'/>
        <meta name='description' content='Have questions on car repairs, maintenance, and parts? Get answers to common questions at $DealerName.'/>";
        break;
    case 'contact-information':
        echo "<meta name='Title' content='Contact Us | ${$DealerLocation} Car Repair'/>
        <meta name='description' content='Contact our dealership regarding car repairs and service.'/>";
        break;
    case 'our-staff':
        echo "<meta name='Title' content='Our Staff | ${$DealerLocation} Car Repair'/>
        <meta name='description' content='Want to contact a member of our service center&apos;s staff? See our full staff list here.'/>";
        break;
}
